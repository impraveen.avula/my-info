---
title: 'About Me'
avatar: './me.jpeg'
skills:
  - JavaScript (ES6+)
  - HTML & CSS
  - Bootstrap 4
  - JQuery
  - Vue.js
  - Angular.js
  - Node.js
  - Express
  - Mongo DB
  - Flutter
  - PHP
  - MySQL
---

Hello! I'm Varun, a Product Developer particularly a Node.js developer, I enjoy building things that live on the internet. I develop exceptional websites, web apps and mobile apps that provide intuitive,pixel-perfect user interfaces with efficient and modern backends.

Shortly, Working hard for my Computer Science Degree [Bharath University](https://bharathuniv.ac.in/), I worked with a Startup for more than five continous months during Second year of my graduation, I work on a wide variety of interesting and meaningful projects.

Here are a few technologies I've been working with:
