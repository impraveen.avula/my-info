---
date: '3'
title: 'Skill Launcher'
cover: './skill.jpeg'
external: 'https://play.google.com/store/apps/details?id=com.learn.skill_launcher'
tech:
  - Flutter
  - PHP
  - MySQL
showInProjects: true
---

Its an E Learning Platform where students can enroll for their desired technology and get certified after a few succesful quiz attempts. Released for Android and iOS.
