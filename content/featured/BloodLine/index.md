---
date: '4'
title: 'Blood Line'
cover: './bline.png'
github: 'https://github.com/loserscorp/tree'
external : "https://bline.netlify.com"
tech:
  - Vue.js
  - Bootstrap 4
  - Node.js
  - Mongo DB
showInProjects: true
---
Using this web app anyone can create their family tree with hassle free login system. It also provides some crazy analytics for their respective tree and much more. We provided all premium features with lifetime free access.