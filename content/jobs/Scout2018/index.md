---
date: '2019-06-08'
title: 'Product Engineer'
company: 'Techimax'
location: 'Hyderabad, Kondapur'
range: 'June 2019 - October 2019'
url: 'https://www.techimax.in/'
---

- Developed a Mobile e-Learning Platform for Android nad iOS - [SkillLauncher](https://play.google.com/store/apps/details?id=com.learn.skill_launcher)
- Coded an iOS application using a Cross Platform Framework with end to end encryption and pushed to AppStore - [VTip](https://apps.apple.com/in/app/vtip/id1478514502)
- Worked with a variety of technologies like Javascript, Flutter, Cloud Functions and MongoDB.
- Communicate with multi-disciplinary teams of engineers, designers, producers, and clients.
