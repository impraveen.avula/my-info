---
date: '2019-11-10'
title: 'Technical Associate'
company: 'BIHER Incubation'
location: 'Chennai'
range: 'November 2019 - Present'
url: 'https://www.bharathuniv.in/'
---

- Developed an E Learning cross platform application.
- Published a Nodejs backed Electron app.
- Trained 100+ Students to build mobile apps from scratch.
